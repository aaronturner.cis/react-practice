import {
    FETCH_ALBUM_DATA_REQUEST,
    FETCH_ALBUM_DATA_SUCCESS,
    FETCH_ALBUM_DATA_FAILURE,
} from "../../types/albumData";

const initialState = {
    loading: true,
    data: [],
    error: "",
};
const albumDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUM_DATA_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case FETCH_ALBUM_DATA_SUCCESS:
            return {
                loading: false,
                data: action.data,
                error: "",
            };
        case FETCH_ALBUM_DATA_FAILURE:
            return {
                loading: false,
                data: [],
                error: action.data,
            };
        default:
            return state;
    }
};

export default albumDataReducer;
