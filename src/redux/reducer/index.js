import { combineReducers } from "redux";
import albumDataReducer from "./albumData/albumData";

const index = combineReducers({
  albumData: albumDataReducer,
});

export default index;
