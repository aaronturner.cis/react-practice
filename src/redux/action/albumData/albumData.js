import axios from "axios";

import {
    FETCH_ALBUM_DATA_REQUEST,
    FETCH_ALBUM_DATA_SUCCESS,
    FETCH_ALBUM_DATA_FAILURE,
} from "../../types/albumData";

export const fetchAlbumDataRequest = () => ({
    type: FETCH_ALBUM_DATA_REQUEST,
});

export const fetchAlbumDataSuccess = (data) => ({
    type: FETCH_ALBUM_DATA_SUCCESS,
    data: data,
});


export const fetchAlbumDataFailure = (error) => ({
    type: FETCH_ALBUM_DATA_FAILURE,
    data: error,
})

export const fetchAlbumData = () => (dispatch) => {
    dispatch(fetchAlbumDataRequest);
    return axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((response) => {
            return dispatch(fetchAlbumDataSuccess(response.data));
        })
        .catch((error) => {
            return dispatch(fetchAlbumDataFailure(error.message));
        });
};

