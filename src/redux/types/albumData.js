export const FETCH_ALBUM_DATA_REQUEST = "FETCH_ALBUM_DATA_REQUEST"
export const FETCH_ALBUM_DATA_SUCCESS = "FETCH_ALBUM_DATA_SUCCESS"
export const FETCH_ALBUM_DATA_FAILURE = "FETCH_ALBUM_DATA_FAILURE"