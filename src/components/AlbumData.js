import classNames from 'classnames';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Input, InputGroup, Table } from 'reactstrap'
import { fetchAlbumData } from '../redux/action/albumData/albumData';
import NavBar from './common/NavBar'


const AlbumData = () => {

  const dispatch = useDispatch()
  // const albumData = useSelector((state) => state?.albumData?.data);

  const [data, setData] = useState()
  const [dataPerPage, setDataPerPage] = useState(10)
  const [currentPage, setCurrentPage] = useState(1)
  const [searchTerm, setSearchTerm] = useState("")
  const [searchField, setSearchField] = useState("")
  const [sortOrder, setSortOrder] = useState("DSC")
  const [sortedField, setSortedField] = useState("")

  useEffect(() => {
    getData()
  }, [])

  const getData = () => {
    dispatch(fetchAlbumData()).then((data) => {
      setData(data?.data)
    })
  }

  const getFilterData = () => {
    if (searchTerm) {
      const value = searchTerm.toLowerCase();
      if (searchField) {
        const fieldSearch = data?.filter(
          (d) => {
            const fieldArray = d[searchField]
            const fieldString = fieldArray.toString().toLowerCase().search(value)
            return fieldString !== -1
          }
        );
        return fieldSearch
      } else {
        const searchedData = data?.filter(
          (d) => {
            const valueArray = Object.values(d).toString().toLowerCase().search(value)
            return valueArray !== -1
          }
        );
        return searchedData
      }
    } else return data;
  };

  const filterData = getFilterData()

  const sortField = () => {
    console.log('Col', sortedField)
    if (sortOrder === "ASC") {
      const sortedData = data?.sort((a, b) => {
        console.log(a)
        return a[sortedField] > b[sortedField] ? 1 : -1
      }
      );
      setSortOrder("DSC")
      setData(sortedData)
      console.log(sortedData)
    }
    if (sortOrder === "DSC") {
      const sortedData = data?.sort((a, b) =>
        a[sortedField] < b[sortedField] ? 1 : -1
      );
      console.log(sortedData)
      setData(sortedData)
      setSortOrder("ASC")
    }
  }

  const Pagination = () => {
    if (data && (filterData.length > dataPerPage)) {
      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(data?.length / dataPerPage); i++) {
        pageNumbers.push(i);
      }
      return (
        <>
          <nav className="ms-4" style={{ maxWidth: 50 }}>
            <ul className="pagination">
              {pageNumbers.map((number) => (
                <li
                  key={number}
                  className={classNames({
                    "page-item": true,
                    active: currentPage === number,
                  })}
                >
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() => setCurrentPage(number)}
                    className="page-link "
                  >
                    {number}
                  </span>
                </li>
              ))}
            </ul>
          </nav>
        </>
      );
    } else return null
  };

  const getTableData = (dataPerPage, currentData, filterData) => {
    if (dataPerPage !== "All") {
      return currentData;
    }
    return filterData;
  };

  const indexOfLastData = currentPage * dataPerPage;
  const indexOfFirstData = indexOfLastData - dataPerPage;
  const currentData = filterData?.slice(indexOfFirstData, indexOfLastData);
  //For Table Data
  const tableData = getTableData(dataPerPage, currentData, filterData);


  return (
    <><div>
      <NavBar />
    </div>
      <div style={{ marginTop: 75, textAlign: 'center' }}><h3>Album Data</h3></div>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <span className="ms-4" style={{ flexDirection: 'row' }}>
          Show {"\u00A0"}
          <select
            onChange={(e) => { setDataPerPage(e.target.value); setCurrentPage(1) }}
            name="dataPerPage"
            id="dataPerPage"
          >
            <option value={10}>10</option>
            <option value={50}>50</option>
            <option value={100}>100</option>
            <option value={1000}>1000</option>
            <option value={"All"}>All Data</option>
          </select>
          {"\u00A0"}
          Data
          <Input style={{ maxWidth: 300, flexDirection: 'row' }} placeholder='Search...' onChange={(e) => { setSearchTerm(e.target.value); setCurrentPage(1); setSearchField("") }} />
        </span>
      </div>
      {
        data &&
        <div style={{ marginTop: 20 }} className="ms-4">
          <Table
            bordered
            hover
            responsive
            striped
          >
            <thead>{data.length > 0 &&
              <tr>
                {Object?.keys(data[0])?.map((header, i) => {
                  return (
                    <th key={i} style={{ textAlign: 'center' }} >
                      <span onClick={() => { setSortedField(header); sortField() }}>{header}{" "}</span>
                      {(sortedField === header) && (sortOrder === "ASC") && <span>🢁</span>}
                      {(sortedField === header) && (sortOrder === "DSC") && <span>🢃</span>}
                      <InputGroup>
                        <Input placeholder='Search...' onChange={(e) => {
                          setSearchField(header)
                          setSearchTerm(e.target.value)
                          setCurrentPage(1)
                        }} />
                      </InputGroup>
                    </th>
                  )
                })}
              </tr>
            }
            </thead>
            <tbody>

              {tableData ?
                <>
                  {tableData?.map((album, i) => {
                    return <tr key={i}>{Object?.values(album).map((d, i) => {
                      return (
                        <td key={i}>{d}</td>
                      )
                    })}</tr>
                  })}
                </> :
                <tr><td>No Data Available</td></tr>
              }


              {/* {tableData?.map((album, i) => {
                return <tr key={i}>{Object?.values(album).map((d, i) => {
                  return (
                    <td key={i} style={{ textAlign: 'center' }}>{d}</td>
                  )
                })}</tr>
              })} */}
            </tbody>
          </Table>
          <Pagination />
        </div>
      }
    </>
  )
}

export default AlbumData