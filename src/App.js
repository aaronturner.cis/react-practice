import './App.css';
import { Provider } from "react-redux";
import store from './redux/store';
import AlbumData from './components/AlbumData';

function App() {
  return (
    <Provider store={store}>
      <AlbumData />
    </Provider>
  );
}

export default App;
